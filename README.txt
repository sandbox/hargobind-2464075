
-- SUMMARY --

Works for D6.

Adds contextual links for several objects like nodes, views, blocks etc.
It's also an API which other modules can hook into to add or alter
extra links.

You need to add an extra variable to your template files:

- block: $block_classes
- node: $node_classes
- user: $block_classes
- views: $view_classes
- page: (the page content is actually wrapped in a <div> containing
    the appropriate classes, so there is nothing you need to do)

